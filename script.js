$(document).ready(function() {
    if (localStorage.getItem("login") !== null) {
        if (localStorage.getItem("login") == 1) {
            $('.center_cont').hide();
            $('.hello').css('display', 'flex');
        }
    }
    $("#login-form").submit(function() {
        var pass = $("#lock_input").val();
        var email = $("#usr_input").val();
        $('div.answer').hide();
        $.post("http://shelkovica.beget.tech/get_data.php", { NAME: email, PASS: pass },
            function(json) {
                console.log(json);
                if (json.RESULT != 'OK') {
                    //error
                    $('div.answer').html(json.RESULT);
                    $('div.answer').show();
                } else {
                    //save session
                    localStorage.setItem('login', 1); // For test only!
                    $('.center_cont').hide();
                    $('.hello').css('display', 'flex');
                }
            }, 'json')
        return false;
    });
    $('a#logout').on('click', function() {
        localStorage.setItem('login', 0); // For test only!
        $('.center_cont').show();
        $('.hello').hide();
    });
});
